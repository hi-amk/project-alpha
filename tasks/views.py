from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from tasks.models import Task
from django.views.decorators.http import require_http_methods
from datetime import datetime
from tasks.models import STATUS_CHOICES
from tasks.forms import CommentForm


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/new.html"
    fields = ["name", "project", "start_date", "due_date", "assignee", "notes"]

    def get_success_url(self) -> str:
        return reverse_lazy("show_project", args=[self.object.project.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["STATUS_CHOICES"] = STATUS_CHOICES
        return context

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


@require_http_methods(["POST"])
def complete_task(request, pk):
    if request.method == "POST":
        task = Task.objects.get(id=pk)
        task.is_completed = True
        task.date_completed = datetime.now()
        task.status = "CLOSED"
        task.save()
    return redirect("show_my_tasks")


def update_status(request, pk):
    if request.method == "POST":
        task = Task.objects.get(id=pk)
        task.status = request.POST.get("updated_status")
        if task.status in ["DROPPED", "COMPLETED", "CLOSED"]:
            task.is_completed = True
            task.date_completed = datetime.now()
        elif task.status in [
            "OPEN",
            "IN PROGRESS",
            "PENDING",
            "NEEDS FOLLOW UP",
        ]:
            task.is_completed = False
            task.date_completed = None
        task.save()
    return redirect("show_my_tasks")


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "tasks/edit.html"
    fields = [
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "date_completed",
        "project",
        "assignee",
        "notes",
        "status",
        "priority",
    ]
    success_url = reverse_lazy("show_my_tasks")
    # def get_success_url(self) -> str:
    #     return reverse_lazy("")

    # We want other people to be able to edit other people's task
    # (e.g. manager updating managee's task)
    # def get_queryset(self):
    #     return Task.objects.filter(assignee=self.request.user)


class TaskDetailView(LoginRequiredMixin, DetailView):
    model = Task
    template_name = "tasks/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["comment_form"] = CommentForm()

        return context

    # We want other people to be able to view other people's task details
    # (e.g. manager viewing and commenting on managee's task, or even teammates)
    # def get_queryset(self):
    #     return Task.objects.filter(assignee=self.request.user)


def post_comment(request, pk):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.task = Task.objects.get(pk=pk)
            comment.commenter = request.user
            comment.save()
    return redirect("show_task", pk=pk)


def search_task(request):
    """search function"""
    if request.method == "POST":
        query_name = request.POST.get("name", None)
        tasks = Task.objects.all()
        if query_name:
            results = Task.objects.filter(name__contains=query_name)
            return render(
                request,
                "tasks/task_search.html",
                {
                    "results": results,
                    "STATUS_CHOICES": STATUS_CHOICES,
                    "query_name": query_name,
                }
                # request,
                # "tasks/list.html",
                # {"results": results, "tasks": tasks},
            )

    return render(request, "tasks/task_search.html")
    # return render(request, "tasks/list.html")
