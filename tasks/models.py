from django.db import models
from django.conf import settings

# from tasks.choices import STATUS_CHOICES

USER_MODEL = settings.AUTH_USER_MODEL

STATUS_CHOICES = (
    ("OPEN", "OPEN"),
    ("IN PROGRESS", "IN PROGRESS"),
    ("PENDING", "PENDING"),
    ("NEEDS FOLLOW UP", "NEEDS FOLLOW UP"),
    ("DROPPED", "DROPPED"),
    ("COMPLETED", "COMPLETED"),
    ("CLOSED", "CLOSED"),
)

PRIORITY_CHOICES = (
    ("URGENT", "URGENT"),
    ("HIGH", "HIGH"),
    ("Normal", "Normal"),
    ("Low", "Low"),
)


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    date_completed = models.DateTimeField(blank=True, null=True)
    project = models.ForeignKey(
        "projects.Project",
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        USER_MODEL,
        related_name="tasks",
        null=True,
        on_delete=models.SET_NULL,
    )
    notes = models.TextField(blank=True, null=True)
    status = models.CharField(
        max_length=15, choices=STATUS_CHOICES, default="OPEN"
    )
    priority = models.CharField(
        max_length=6, choices=PRIORITY_CHOICES, default="Normal"
    )

    def __str__(self):
        return self.name


# https://djangocentral.com/creating-comments-system-with-django/
class Comment(models.Model):
    task = models.ForeignKey(
        "Task", related_name="comments", on_delete=models.CASCADE
    )
    commenter = models.ForeignKey(
        USER_MODEL,
        related_name="comments",
        null=True,
        on_delete=models.PROTECT,
    )
    body = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["created_on"]

    def __str__(self):
        return "Comment: " + str(self.body) + " by " + str(self.commenter)
