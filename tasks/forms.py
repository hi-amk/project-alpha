from tasks.models import Comment
from django import forms


class CommentForm(forms.ModelForm):
    # https://docs.djangoproject.com/en/4.0/topics/forms/modelforms/#overriding-the-default-fields
    # These 3 lines below have exactly the same effect as lines 14-17
    # body = forms.CharField(
    #     label="", widget=forms.Textarea(attrs={"rows": 4, "cols": 120})
    # )

    class Meta:
        model = Comment
        fields = ["body"]
        labels = {"body": ""}
        widgets = {
            "body": forms.Textarea(attrs={"rows": 4, "cols": 130}),
        }
