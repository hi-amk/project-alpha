from django.urls import path
from tasks.views import (
    TaskCreateView,
    TaskListView,
    complete_task,
    update_status,
    TaskUpdateView,
    TaskDetailView,
    post_comment,
    search_task,
)


urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", complete_task, name="complete_task"),
    path("<int:pk>/status/", update_status, name="update_status"),
    path("<int:pk>/edit/", TaskUpdateView.as_view(), name="update_task"),
    path("<int:pk>/", TaskDetailView.as_view(), name="show_task"),
    path("<int:pk>/comment/", post_comment, name="post_comment"),
    path("search/", search_task, name="search_task"),
]
