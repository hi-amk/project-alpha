ORDER

1. Project Detail View: Show Members
2. Show Project name in Task Detail view
3. Date Completed
4. Notes in Task Detail View
5. Progress degrees
6. Priority
7. Task Update View
8. Task Detail View
9. Comments in Task Detail View https://djangocentral.com/creating-comments-system-with-django/
10. Project status: ONGOING VS FINISHED
11. SEARCH FEATURE?! https://pytutorial.com/django-search-function, https://www.youtube.com/watch?v=G-Rct7Na0UQ
12. Project Completion %
13. CSS and minor improvements
14. In Task ListView: If past due date, Date in Due Date show with red font
15. Members' pictures/identifier icon

DETAILS

1. Project Detail View: Show Members
  a) App: Projects (no change)
  b) View: Need to change to add to context??? Not sure.
  c) N/A
  d) Template changes:
    i) just show on Project Detail View

2. Show Project name in Task Detail View

3. Date Completed
  a) App: Tasks
  b) DateTime Field in existing Task Model
  c) Notes:
  d) Template changes:
    i) show to the right of 'Is completed' in Tasks list view
    ii) show in Tasks list view

4. Notes in Task Detail View
  a) App: Tasks
  b) TextFiled within Task Model
    i) blank=True so it's not a required field when user creates the task
    ii) null=True so it's ok that database is blank
  c) Notes: (One TextField for general notes)

5. Progress degrees
  a) App: Tasks
  b) Separate Model in tasks/models.py
  c) Notes:
    i) Open (DEFAULT)
    ii) In Progress
    iii) Pending
    iv) Needs Follow Up
    v) Dropped
    vi) Completed
    vii) Closed
  d) Template changes

6. Priority
  a) App: Tasks
  b) Separate Model in tasks/models.py
  c) Notes:
    i) Urgent
    ii) High
    iii) Normal (DEFAULT)
    iv) Low

7. Task Update View
  a) App: Tasks
  b) N/A
  c) Notes:
  d) Template changes:

8. Task Detail View
  a) App: Tasks
  b) N/A
  c) Notes:
  d) Template changes:
    i) show notes

9. Comments in Task Detail View
  https://djangocentral.com/creating-comments-system-with-django/
  a) App: Tasks
  b) Separate Model in Tasks/models.py
  c) Notes: (Separate Model within Tasks App, for members to add comments)


10. Project status: ONGOING VS FINISHED
  a) if all the tasks in the project are is_completed == True, then show FINISHED
  b) if not all the tasks in the project are is_completed == False, then show ONGOING


project.tasks.all.is_completed


{% comment %} {% if False in project.tasks.all.is_completed %}
                    <h2>ONGOING</h2>
                {% else %}
                    <h2>FINISHED</h2>
                {% endif %} {% endcomment %}

11. SEARCH FEATURE?!
https://pytutorial.com/django-search-function
https://www.youtube.com/watch?v=G-Rct7Na0UQ
  a) for both PROJECT search,
  b) and TASK search

12. Project Completion %

13. CSS and minor improvements

14. In Task ListView: If past due date AND Is Completed == False, then date in Due Date show with red bold font

15. Members' pictures/identifier icon


Improvements for later:
1. CSS:
  X a) In Project DetailView, show members in 3 columns instead of just 1 column
  V b) In My Tasks view (tasks/mine/) Adjust height spacing so "Done" and "Complete" (all rows) have same height
  V c) In TaskDetailView html, make the detail view look nice....
  V d) In TaskDetailView html/css, make the Notes and Comments look nice lol


2. In My Tasks view, when a task's "Is Completed" is updated from Done back to Complete in the admin,
the Date Completed should become blank, and
the Status should become OPEN
But they do not. Would be nice to program so that when Is Completed == False, then the two above are updated accordingly.

V 3. Update ProjectDetailView to show progress?

V 4. Task DetailView: Notes to include Markdownify

5. In Task model, do I want to change fieled "is_completed" to "is_closed"?
Also, "date_completed" to "date_closed"?