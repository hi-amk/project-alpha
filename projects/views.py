from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from projects.models import Project


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # This is when I had calculated the completion rate in the view
        # completion_rate_per_project = {}
        # for project in Project.objects.all():
        #     completed_tasks = 0
        #     tasks = project.tasks.all()
        #     for task in tasks:
        #         if task.is_completed == True:
        #             completed_tasks = completed_tasks + 1
        #     # taking care of division by 0
        #     if len(tasks) == 0:
        #         completion_rate_per_project[project.id] = 0
        #     else:
        #         completion_rate_per_project[
        #             project.id
        #         ] = completed_tasks / len(tasks)

        # context["completion_rate_per_project"] = completion_rate_per_project

        # When I couldn't figure out how to display that 0.0-1.0 rate value from above code in % form in the webpage,
        # I found the widthratio Django filter that CALCULATES percentages in addition to displaying them
        # so instead of making the RATE available in the context dictionary,
        # the below makes the NUMBER OF COMPLETED TASKS in the context dictionary
        completed_tasks_per_project = {}
        for project in Project.objects.all():
            completed_tasks = 0
            tasks = project.tasks.all()
            for task in tasks:
                if task.is_completed == True:
                    completed_tasks = completed_tasks + 1

            completed_tasks_per_project[project.id] = completed_tasks

        context["completed_tasks_per_project"] = completed_tasks_per_project

        return context

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        project_completion_status = True
        for task in self.object.tasks.all():
            if task.is_completed == False:
                project_completion_status = False
                break
        context["project_completion_status"] = project_completion_status

        completed_tasks = 0
        tasks = self.object.tasks.all()
        for task in tasks:
            if task.is_completed == True:
                completed_tasks = completed_tasks + 1
        context["completed_tasks"] = completed_tasks

        return context


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/new.html"
    fields = ["name", "description", "members"]

    def get_success_url(self) -> str:
        return reverse_lazy("show_project", args=[self.object.id])


def search_project(request):
    """search function"""
    if request.method == "POST":

        query_name = request.POST.get("name", None)

        completed_tasks_per_project = {}
        for project in Project.objects.all():
            completed_tasks = 0
            tasks = project.tasks.all()
            for task in tasks:
                if task.is_completed == True:
                    completed_tasks = completed_tasks + 1

            completed_tasks_per_project[project.id] = completed_tasks

        projects = Project.objects.all()
        if query_name:
            results = Project.objects.filter(name__contains=query_name)
            return render(
                request,
                "projects/project_search.html",
                {
                    "results": results,
                    "query_name": query_name,
                    "completed_tasks_per_project": completed_tasks_per_project,
                }
                # request,
                # "projects/list.html",
                # {"results": results, "projects": projects},
            )

    return render(request, "projects/project_search.html")
    # return render(request, "projects/list.html")
